fun func1 (arr: Array<Int>): Int {
    if(arr.count() < 3) // 0, 1, 2. we need at least third index(2)
        return 0
    var sum = 0
    var count = 0
    for ((index, value) in arr.withIndex()) {
        if(index != 0 && index % 2 == 0){
            sum += value
            count++
        }
    }
    return sum / count
}

fun func2(str: String): Boolean {
    if (str.length < 2) return true
    val len = str.length
    var i = 0
    while (i < (len / 2)){
        if(str[i] != str[(len - 1) - i]){
            return false
        }
        i += 1
    }
    return true
}

fun main() {
    // Task 1
    val arr = arrayOf(1, 2, 3, 4, 5)
    println(func1(arr))

    // Task 2
    println("abcba " + func2("abcba"))
    println("abcbc " + func2("abcbc"))
    println("axcba " + func2("axcba"))
    println("a " + func2("a"))
    println("aa " + func2("aa"))
    println("ab " + func2("ab"))
}